//-------------------------------
//         Move the box
//-------------------------------
//
/*
instructions:
  Write a script that will animate the box
  move it 200 pixels to the right over the course of 2 seconds.

Restrictions:
  Don't use any external libraries
  Don't change the html or CSS
  Vanilla JS only :)
*/

const box = document.querySelector(".box");
// const boxOGlocation = box.getBoundingClientRect();
const duration = "2000ms";
const changeX = "200px";

box.style.transition= `transform ${duration} linear`;
box.style.transform= `translateX(${changeX})`;